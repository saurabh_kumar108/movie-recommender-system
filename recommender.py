import operator
try:
    # for Python2
    from Tkinter import *
except ImportError:
    # for Python3
    from tkinter import *

NUMOFUSERS=671
NUMOFMOVIES=9125
SIMILARUSERS=7



def getSimilarityScore(vec1,vec2):
    v1=vec1
    v2=vec2
    mean=(0.5+5.0)/2.0
    score=0.0
    for i in range(NUMOFMOVIES):
        if vec1[i] != 0 and vec2[i] != 0:
            score+=(mean-abs(vec1[i]-vec2[i]))

    return (score/(((len(list(filter((0).__ne__, v1))))+(len(list(filter((0).__ne__, v2)))))))




def getRatedMovies(ratings,user,r=0):
    movieList=[]
    for i in range(NUMOFMOVIES):
        if(ratings[user][i]>r):
            movieList.append(reverseMap[i])

    return set(movieList)

def getWatchedMovies(movieData,ratings,user):
    st=''
    for i in range(NUMOFMOVIES):
        if(ratings[user][i]>0):
            st = st + "  " +movieData[reverseMap[i]][1] + "    "+ repr(ratings[user][i]) +"\n"
           
    rated.delete('1.0', END)
    rated.insert(INSERT,st)


#one row per user. columns are movies.
#column number for a particular id is 0th entry for that is in the movieData dictionary
ratings=[[0]*NUMOFMOVIES for i in range(NUMOFUSERS)]

movieData={}
#maps ratings array index back to movie ID
reverseMap={}

count=0
with open("movies.csv",encoding="utf8") as inputFile:
    for line in inputFile:
        if count==0:
            count+=1
            continue
        st=line
        content=st.split(',')
        movieID=int(content[0])
        content[0]=count-1
        #content.append(movieID)
        reverseMap[content[0]]=movieID
        movieData[movieID]=content
        count+=1



count=0
with open("ratings.csv",encoding="utf8") as inputFile:
    for line in inputFile:
        if count==0:
            count+=1
            continue
        
        st=line
        content=st.split(',')
        ratings[int(content[0])-1][movieData[int(content[1])][0]]=float(content[2])


def mainFun():
    try:
        user=int(e.get())-1 #query response -1. get from gui

        getWatchedMovies(movieData,ratings,user) # show already rated movies to GUI


        
        scores={}
        for index in range(NUMOFUSERS):
            if index==user:
                continue
            scores[index]=getSimilarityScore(ratings[index],ratings[user])

        #sort dictionary based on values

        scores=sorted(scores.items(),key=operator.itemgetter(1),reverse=True)

        #now get movies not watched by user and display them


        recommendations=[]
        recommendations=set(recommendations)
        watched=getRatedMovies(ratings,user)
       # print(watched)
        count=0

        similarUsers=[]
        for i in range(len(scores)):
            count+=1
            if(scores[i][1]>0):
                tempSet=getRatedMovies(ratings,i)
                recommendations=recommendations | tempSet
                similarUsers.append(i)
    
            if count==SIMILARUSERS or scores[i][1]<=0.0:
                break


        recommendations=recommendations - watched
        recommendations=list(recommendations)
        recco=[]
        for x in range (len(recommendations)):
            points=0
            for y in similarUsers:
                #print(recommendations[x])
                if(ratings[y][movieData[recommendations[x]][0]]==0):
                    points+=0.1
                points+=ratings[y][movieData[recommendations[x]][0]]

            recco.append([points/len(similarUsers),recommendations[x]])

        
        recco=sorted(recco,reverse=True)
                    

        
        st=''
        #show recommended movies to GUI
        count=0
        for index in range(len(recco)):
            count+=1
            st = st +" "+movieData[recco[index][1]][1] + "    "+ movieData[recco[index][1]][-1] +"\n"
            if(count==5):
                break

        textPad.delete('1.0', END)
        textPad.insert(INSERT,st)

    except:
        st="Valid users are numbers 1 to 671"
        textPad.delete('1.0', END)
        textPad.insert(INSERT,st)
        rated.delete('1.0', END)
        rated.insert(INSERT,st)





master = Tk()
L1 = Label(master, text="Enter User")
L1.pack()
e = Entry(master, width=80)
e.pack()
b = Button(master, text="Get Recommendations", command=mainFun)
b.pack()
L2 = Label(master, text="Rated Movies")
L2.pack()
rated = Text(master, width=100, height=20)
rated.pack()
L3 = Label(master, text="Recommended Movies")
L3.pack()
textPad = Text(master, width=100, height=20)
textPad.pack()


master.mainloop()

